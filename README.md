Requisitos
--------------------------------------------------------------------------------
-python 2.7

-pip version >= 10.0

-package wheel

    pip install wheel

Creación de paquete
--------------------------------------------------------------------------------
1.- Crear paquete
--------------------------------------------------------------------------------
orden de carpetas

Carpeta Principal "Nombre"

-------Paquete Python "Package"

--------- __ init __.py

--------- class_name.py

--------- class_name.py

#opcional
README.md

LICENSE.txt

setup.cfg

setup.py


2.- crear archivo setup.py
--------------------------------------------------------------------------------
from setuptools import setup

setup(

name='Package',

version='1.0',

description='Clases para el PLN',

author='Ulises Garcia',

author_email='ugarciacalderon@gmail.com',

url='https://gitlab.com/ugarciacalderon/Servicio_Social.git',

license='GPL',

packages=['Package']

)

3.- Construir paquete .whl
--------------------------------------------------------------------------------
python setup.py bdist_wheel

creara automaticamente 3 carpetas

-build

-dist

-Package.egg-info

4.- Entrar a carpeta dist, encontraremos el paquete .whl
--------------------------------------------------------------------------------
5.- instalaremos el paquete .whl usando el comando 
--------------------------------------------------------------------------------
pip install nombre_del_paquete.whl

6.- Mostrara un mensaje en terminal diciendo que el paquete ha sido instalado  correctamente
--------------------------------------------------------------------------------
7.-Podremo utilizar el paquetes con sus modulos haciendo uso de un id Python
--------------------------------------------------------------------------------
 importaremos la clase que deseamos utilizar
 ejemplo:
 
 from Package.ManejoArchivos import *
 from Package.ManejoER import *
 from Package.MyListArgs import *
 
 #creación del objeto
 
 obj = ManejoArchivos()
 
 #uso de un modulo
 
 obj.crearcarpetas(path="/home/ugarciacalderon/Desktop")
 
 
 Más info en
 -------------------------------------------------------------------------------
 https://packaging.python.org/tutorials/distributing-packages/
 
 https://stackoverflow.com/questions/27885397/how-do-i-install-a-python-package-with-a-whl-file